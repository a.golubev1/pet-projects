package ru.cft.shift.task3.app.model;

public enum DifficultyType {
    NOVICE,
    MEDIUM,
    EXPERT
}
