package ru.cft.shift.task3.view;

public interface TimerListener {
    void update(int time);
}
