package ru.cft.shift.task3.app.model;

import ru.cft.shift.task3.view.TimerListener;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class GameTimer extends Thread{
    private int time = 0;
    private final ScheduledExecutorService timer = Executors.newScheduledThreadPool(1);
    private ScheduledFuture<?> timerHandler;
    private TimerListener timerListener;

    public void reset(){
        finish();
        time = 0;
        timerListener.update(0);
    }

    public void start() {
        Runnable task = () -> {
            time++;
            timerListener.update(time);
        };
        timerHandler = timer.scheduleAtFixedRate(task, 0, 1, TimeUnit.SECONDS);
    }

    public void finish() {
        timerHandler.cancel(true);
    }

    public int getTime() {
        return time;
    }

    public void setTimeAction(TimerListener listener) {
        this.timerListener = listener;
    }
}
