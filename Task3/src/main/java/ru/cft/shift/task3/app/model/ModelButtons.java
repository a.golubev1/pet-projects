package ru.cft.shift.task3.app.model;

public enum ModelButtons {
    LEFT_BUTTON,
    RIGHT_BUTTON,
    MIDDLE_BUTTON,
}
