package ru.cft.shift.task3.view;

public interface IsRecordListener {
    void update(boolean isRecord);
}
