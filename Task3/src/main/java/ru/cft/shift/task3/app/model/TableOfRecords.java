package ru.cft.shift.task3.app.model;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.cft.shift.task3.view.IsRecordListener;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class TableOfRecords {
    private ArrayList<Record> records;
    private IsRecordListener recordListener;
    private DifficultyType difficultyOfRecord;

    public TableOfRecords() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            records = new ArrayList<>();
            difficultyOfRecord = null;
            File fileWithRecords = new File("Task3/src/main/resources/records.json");
            if (!fileWithRecords.isFile()) {
                PrintWriter writer = new PrintWriter(fileWithRecords);
                writer.println(" [{\"name\" : \"UNKNOWN\", \"time\":9999}, {\"name\" : \"UNKNOWN\", \"time\":9999}, " +
                        "{\"name\" : \"UNKNOWN\", \"time\":9999}]");
                writer.close();
            }
            records = objectMapper.readValue(fileWithRecords, new TypeReference<>() { });
            /*for(LinkedHashMap record : recordsTemp) {
                Record newRecord = new Record((String) record.get("name"), (Integer) record.get("time"));
                records.add(newRecord);
            }*/
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public void checkRecords(DifficultyType difficulty, int time) {
        switch (difficulty) {
            case NOVICE:
                if (records.get(0).getTime() > time) {
                    difficultyOfRecord = DifficultyType.NOVICE;
                    recordListener.update(true);
                }
                break;
            case MEDIUM:
                if (records.get(1).getTime() > time) {
                    difficultyOfRecord = DifficultyType.MEDIUM;
                    recordListener.update(true);
                }
                break;
            case EXPERT:
                if (records.get(2).getTime() > time) {
                    difficultyOfRecord = DifficultyType.EXPERT;
                    recordListener.update(true);
                }
                break;
            default:
                break;
        }

    }

    public void applyNewRecord(String name, int time) {
        switch (difficultyOfRecord) {
            case NOVICE:
                records.get(0).setName(name);
                records.get(0).setTime(time);
                break;
            case MEDIUM:
                records.get(1).setName(name);
                records.get(1).setTime(time);
                break;
            case EXPERT:
                records.get(2).setName(name);
                records.get(2).setTime(time);
                break;
            default:
                break;
        }
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(new File("Task3/src/main/resources/records.json"), records);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public ArrayList<Record> getRecords() {
        return records;
    }

    public void setRecordAction(IsRecordListener listener) {
        this.recordListener = listener;
    }
}
