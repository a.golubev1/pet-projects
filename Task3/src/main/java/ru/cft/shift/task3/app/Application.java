package ru.cft.shift.task3.app;

import ru.cft.shift.task3.app.model.ClickResult;
import ru.cft.shift.task3.app.model.GameState;
import ru.cft.shift.task3.app.model.GameTimer;
import ru.cft.shift.task3.app.model.TableOfRecords;
import ru.cft.shift.task3.view.*;

import java.util.concurrent.atomic.AtomicBoolean;

public class Application {
    private static void newGameAfterLoseOrWin(GameState game, SettingsWindow settingsWindow, MainWindow mainWindow,
                                              AtomicBoolean initialClick, GameTimer gameTimer) {
        game.startNewGame(GameType.convertDifficultyFromViewToModel(settingsWindow.getGameType()));
        mainWindow.createGameField(game.getField().length, game.getField()[0].length);
        mainWindow.setTimerValue(0);
        mainWindow.setBombsCount(game.getNumberOfBombs());
        initialClick.set(true);
        gameTimer.reset();
    }

    public static void main(String[] args) {
        MainWindow mainWindow = new MainWindow();
        SettingsWindow settingsWindow = new SettingsWindow(mainWindow);
        HighScoresWindow highScoresWindow = new HighScoresWindow(mainWindow);

        GameState game = new GameState();
        GameTimer gameTimer = new GameTimer();

        game.startNewGame(GameType.convertDifficultyFromViewToModel(settingsWindow.getGameType()));
        mainWindow.createGameField(game.getField().length, game.getField()[0].length);
        mainWindow.setVisible(true);
        mainWindow.setBombsCount(game.getNumberOfBombs());

        TableOfRecords tableOfRecords = new TableOfRecords();
        highScoresWindow.setNoviceRecord(tableOfRecords.getRecords().get(0).getName(), tableOfRecords.getRecords().
                get(0).getTime());
        highScoresWindow.setMediumRecord(tableOfRecords.getRecords().get(1).getName(), tableOfRecords.getRecords().
                get(1).getTime());
        highScoresWindow.setExpertRecord(tableOfRecords.getRecords().get(2).getName(), tableOfRecords.getRecords().
                get(2).getTime());

        AtomicBoolean initialClick = new AtomicBoolean(true);
        mainWindow.setNewGameMenuAction(e -> newGameAfterLoseOrWin(game, settingsWindow, mainWindow, initialClick,
                gameTimer));

        gameTimer.setTimeAction(e -> mainWindow.setTimerValue(gameTimer.getTime()));
        mainWindow.setSettingsMenuAction(e -> settingsWindow.setVisible(true));
        mainWindow.setHighScoresMenuAction(e -> highScoresWindow.setVisible(true));
        mainWindow.setExitMenuAction(e -> mainWindow.dispose());
        tableOfRecords.setRecordAction(e -> {
            RecordsWindow recordsWindow = new RecordsWindow(mainWindow);
            recordsWindow.setNameListener((name) -> {
                tableOfRecords.applyNewRecord(name, gameTimer.getTime());
                switch (settingsWindow.getGameType()) {
                    case NOVICE:
                        highScoresWindow.setNoviceRecord(name, gameTimer.getTime());
                        break;
                    case MEDIUM:
                        highScoresWindow.setMediumRecord(name, gameTimer.getTime());
                        break;
                    case EXPERT:
                        highScoresWindow.setExpertRecord(name, gameTimer.getTime());
                        break;
                }
            });
            recordsWindow.setVisible(true);
        });


        mainWindow.setCellListener((x, y, buttonType) -> {
            if (buttonType == ButtonType.LEFT_BUTTON && initialClick.get()) {
                gameTimer.start();
            }
            mainWindow.setTimerValue(gameTimer.getTime());
            ClickResult resultOfClick = game.cellClick(x, y, initialClick.get(),
                    ButtonType.convertButtonFromViewToModel(buttonType));
            initialClick.set(false);
            if (resultOfClick.getBombs() != null && resultOfClick.getBombs().size() != 0) {
                mainWindow.setCellImage(resultOfClick.getBombs().get(0)[0], resultOfClick.getBombs().get(0)[1],
                        GameImage.BOMB_ICON);
                gameTimer.finish();
                LoseWindow loseWindow = new LoseWindow(mainWindow);

                loseWindow.setNewGameListener(e -> newGameAfterLoseOrWin(game, settingsWindow, mainWindow, initialClick,
                        gameTimer));
                loseWindow.setExitListener(e -> mainWindow.dispose());
                loseWindow.setVisible(true);
            } else if (resultOfClick.isPutFlag()) {
                mainWindow.setCellImage(x, y, GameImage.MARKED);
                mainWindow.setBombsCount(resultOfClick.getNumberOfBombs());
            } else if (resultOfClick.isRemoveFlag()) {
                mainWindow.setCellImage(x, y, GameImage.CLOSED);
                mainWindow.setBombsCount(resultOfClick.getNumberOfBombs());
            } else if (resultOfClick.getEmptyCells() != null) {
                for (Integer[] point : resultOfClick.getEmptyCells()) {
                    mainWindow.setCellImage(point[0], point[1], GameImage.EMPTY);
                }
                for (Integer[] point : resultOfClick.getNumbersCells()) {
                    mainWindow.setCellImage(point[0], point[1], GameImage.valueOf("NUM_" + point[2]));
                }
            } else if (resultOfClick.getNumbersCells() != null) {
                for (Integer[] point : resultOfClick.getNumbersCells()) {
                    mainWindow.setCellImage(point[0], point[1], GameImage.valueOf("NUM_" + point[2]));
                }
            }
            if (resultOfClick.isWin()) {
                gameTimer.finish();
                tableOfRecords.checkRecords(GameType.convertDifficultyFromViewToModel(settingsWindow.getGameType()),
                        gameTimer.getTime());

                WinWindow winWindow = new WinWindow(mainWindow);
                winWindow.setNewGameListener(e -> newGameAfterLoseOrWin(game, settingsWindow, mainWindow, initialClick,
                        gameTimer));
                winWindow.setExitListener(e -> mainWindow.dispose());
                winWindow.setVisible(true);
            }
        });
    }
}
