package ru.cft.shift.task3.app.model;

public class Record {
    private String name;
    private int time;

    public Record() { }

    public Record(String name, int time) {
        this.name = name;
        this.time = time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTime() {
        return time;
    }

    public String getName() {
        return name;
    }
}
