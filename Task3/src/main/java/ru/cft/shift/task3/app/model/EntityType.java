package ru.cft.shift.task3.app.model;

public enum EntityType {
    BOMB,
    FLAG,
    NUMBER,
    OPENED_CELL,
    CLOSED_CELL
}
