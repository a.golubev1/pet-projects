package ru.cft.shift.task3.app.model;

import java.util.ArrayList;

public class ClickResult {
    private final ArrayList<Integer[]> whereBomb;
    private final ArrayList<Integer[]> emptyCells;
    private final ArrayList<Integer[]> numbersCells;
    private final boolean removeFlag;
    private final boolean putFlag;
    private final boolean isWin;
    private final int numberOfBombs;

    public ClickResult(ArrayList<Integer[]> bombCoordinates, ArrayList<Integer[]> empty, ArrayList<Integer[]> numbers,
                       boolean remove, boolean put, boolean win, int newNumberOfBombs) {
        whereBomb = bombCoordinates;
        emptyCells = empty;
        numbersCells = numbers;
        removeFlag = remove;
        putFlag = put;
        isWin = win;
        numberOfBombs = newNumberOfBombs;
    }

    public ArrayList<Integer[]> getEmptyCells() {
        return emptyCells;
    }

    public ArrayList<Integer[]> getNumbersCells() {
        return numbersCells;
    }

    public ArrayList<Integer[]> getBombs() {
        return whereBomb;
    }

    public boolean isPutFlag() {
        return putFlag;
    }

    public boolean isRemoveFlag() {
        return removeFlag;
    }

    public boolean isWin() {
        return isWin;
    }

    public int getNumberOfBombs() {
        return numberOfBombs;
    }
}
