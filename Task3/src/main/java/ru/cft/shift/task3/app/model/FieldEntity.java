package ru.cft.shift.task3.app.model;

public class FieldEntity {
    private EntityType type;

    private int number;

    public void setType(EntityType type) {
        this.type = type;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public EntityType getType() {
        return type;
    }
}
