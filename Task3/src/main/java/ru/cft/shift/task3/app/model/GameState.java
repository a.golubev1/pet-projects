package ru.cft.shift.task3.app.model;


import java.util.ArrayList;
import java.util.Random;


public class GameState {
    private FieldEntity[][] field;
    private FieldEntity[][] openedOrClosedCells;
    private int putedFlags;
    private int numberOfBombs;
    private DifficultyType difficulty;

    public void startNewGame(DifficultyType type) {
        difficulty = type;
        putedFlags = 0;
        switch (difficulty) {
            case NOVICE:
                numberOfBombs = 10;
                fieldGeneration(9, 9, 10);
                break;
            case MEDIUM:
                numberOfBombs = 40;
                fieldGeneration(16, 16, 40);
                break;
            case EXPERT:
                numberOfBombs = 99;
                fieldGeneration(16, 30, 99);
                break;
            default:
                return;
        }
    }

    private void fieldGeneration(int dimensionX, int dimensionY, int numberOfBombs) {
        field = new FieldEntity[dimensionX][dimensionY];
        openedOrClosedCells = new FieldEntity[dimensionX][dimensionY];
        for (int i = 0; i < dimensionX; i++) {
            for (int j = 0; j < dimensionY; j++) {
                field[i][j] = new FieldEntity();
                field[i][j].setType(EntityType.CLOSED_CELL);
                openedOrClosedCells[i][j] = new FieldEntity();
                openedOrClosedCells[i][j].setType(EntityType.CLOSED_CELL);
            }
        }
        while (numberOfBombs != 0) {
            Random random = new Random();
            int bombX = random.nextInt(dimensionX);
            int bombY = random.nextInt(dimensionY);
            if (field[bombX][bombY].getType() != EntityType.BOMB) {
                field[bombX][bombY].setType(EntityType.BOMB);
                numberOfBombs--;
            }
        }
        for (int i = 0; i < dimensionX; i++) {
            for (int j = 0; j < dimensionY; j++) {
                if (field[i][j].getType() != EntityType.BOMB) {
                    createCell(i, j);
                }
            }
        }
    }

    private void createCell(int x, int y) {
        int numberOfNeighborBombs = 0;
        for (int i = x - 1; i < x + 2; i++) {
            for (int j = y - 1; j < y + 2; j++) {
                if ((i == x && j == y) || i < 0 || j < 0 || i >= field.length || j >= field[0].length) {
                    continue;
                }
                if (field[i][j].getType() == EntityType.BOMB) {
                    numberOfNeighborBombs++;
                }
            }
        }
        if (numberOfNeighborBombs != 0) {
            field[x][y].setType(EntityType.NUMBER);
            field[x][y].setNumber(numberOfNeighborBombs);
        } else {
            field[x][y].setType(EntityType.OPENED_CELL);
        }
    }

    private void teleportBomb(int x, int y) {
        field[x][y].setType(EntityType.CLOSED_CELL);
        while (true) {
            Random random = new Random();
            int bombX = random.nextInt(field.length);
            int bombY = random.nextInt(field[0].length);
            if (field[bombX][bombY].getType() != EntityType.BOMB && bombX != x && bombY != y) {
                field[bombX][bombY].setType(EntityType.BOMB);
                break;
            }
        }
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; j++) {
                if (field[i][j].getType() != EntityType.BOMB) {
                    createCell(i, j);
                }
            }
        }
    }

    private ClickResult openCellsNeighborsMiddleClick(int x, int y) {
        ArrayList<Integer[]> emptyCells = new ArrayList<>();
        ArrayList<Integer[]> numberCells = new ArrayList<>();
        ArrayList<Integer[]> neighborsToCheck = new ArrayList<>();
        ArrayList<Integer[]> whereBomb = new ArrayList<>();

        for (int i = x - 1; i < x + 2; i++) {
            for (int j = y - 1; j < y + 2; j++) {
                if ((i == x && j == y) || i < 0 || j < 0 || i >= field.length || j >= field[0].length ||
                        openedOrClosedCells[i][j].getType() == EntityType.FLAG) {
                    continue;
                }
                Integer[] coordinates = new Integer[2];
                coordinates[0] = i;
                coordinates[1] = j;
                neighborsToCheck.add(coordinates);
            }
        }
        for (int k = 0; k < neighborsToCheck.size(); k++) {
            if (field[neighborsToCheck.get(k)[0]][neighborsToCheck.get(k)[1]].getType() ==
                    EntityType.BOMB && openedOrClosedCells[neighborsToCheck.get(k)[0]][neighborsToCheck.get(k)[1]].
                    getType() != EntityType.FLAG) {
                Integer[] bombCoordinates = new Integer[2];
                bombCoordinates[0] = neighborsToCheck.get(k)[0];
                bombCoordinates[1] = neighborsToCheck.get(k)[1];
                whereBomb.add(bombCoordinates);
            }
            emptyOrNumberChecker(emptyCells, numberCells, neighborsToCheck, k);
        }
        return checkForWin(whereBomb, emptyCells, numberCells, false, false);
    }

    private void emptyOrNumberChecker(ArrayList<Integer[]> emptyCells, ArrayList<Integer[]> numberCells,
                                      ArrayList<Integer[]> neighborsToCheck, int k) {
        if (field[neighborsToCheck.get(k)[0]][neighborsToCheck.get(k)[1]].getType() == EntityType.OPENED_CELL) {
            emptyCells.add(neighborsToCheck.get(k));
            openedOrClosedCells[neighborsToCheck.get(k)[0]][neighborsToCheck.get(k)[1]].
                    setType(EntityType.OPENED_CELL);
            for (int i = neighborsToCheck.get(k)[0] - 1; i < neighborsToCheck.get(k)[0] + 2; i++) {
                for (int j = neighborsToCheck.get(k)[1] - 1; j < neighborsToCheck.get(k)[1] + 2; j++) {
                    boolean isAlreadyChecked = false;
                    for (int z = 0; z < neighborsToCheck.size(); z++) {
                        if ((i == neighborsToCheck.get(z)[0] && j == neighborsToCheck.get(z)[1]) || i < 0
                                || j < 0 || i >= field.length
                                || j >= field[0].length) {
                            isAlreadyChecked = true;
                            break;
                        }
                    }
                    if (!isAlreadyChecked) {
                        Integer[] newCellToCheck = new Integer[2];
                        newCellToCheck[0] = i;
                        newCellToCheck[1] = j;
                        neighborsToCheck.add(newCellToCheck);
                    }
                }
            }
        }
        if (field[neighborsToCheck.get(k)[0]][neighborsToCheck.get(k)[1]].getType() == EntityType.NUMBER) {
            Integer[] cellWithNumber = new Integer[3];
            cellWithNumber[0] = neighborsToCheck.get(k)[0];
            cellWithNumber[1] = neighborsToCheck.get(k)[1];
            cellWithNumber[2] = field[neighborsToCheck.get(k)[0]][neighborsToCheck.get(k)[1]].getNumber();
            numberCells.add(cellWithNumber);
            openedOrClosedCells[neighborsToCheck.get(k)[0]][neighborsToCheck.get(k)[1]].
                    setType(EntityType.OPENED_CELL);
        }
    }

    private ClickResult openCellsNeighborsLeftClick(int x, int y) {
        ArrayList<Integer[]> emptyCells = new ArrayList<>();
        ArrayList<Integer[]> numberCells = new ArrayList<>();
        ArrayList<Integer[]> neighborsToCheck = new ArrayList<>();

        Integer[] coordinates = new Integer[2];
        coordinates[0] = x;
        coordinates[1] = y;
        neighborsToCheck.add(coordinates);
        for (int k = 0; k < neighborsToCheck.size(); k++) {
            emptyOrNumberChecker(emptyCells, numberCells, neighborsToCheck, k);
        }
        return checkForWin(null, emptyCells, numberCells, false, false);
    }

    public ClickResult cellClick(int x, int y, boolean isInitialClick, ModelButtons button) {

        if (button == ModelButtons.MIDDLE_BUTTON && field[x][y].getType() == EntityType.NUMBER &&
                openedOrClosedCells[x][y].getType() == EntityType.OPENED_CELL) {
            int numberOfFlagsAround = 0;
            for (int i = x - 1; i < x + 2; i++) {
                for (int j = y - 1; j < y + 2; j++) {
                    if ((i == x && j == y) || i < 0 || j < 0 || i >= field.length || j >= field[0].length) {
                        continue;
                    }
                    if (openedOrClosedCells[i][j].getType() == EntityType.FLAG) {
                        numberOfFlagsAround++;
                    }
                }
            }
            if (field[x][y].getNumber() == numberOfFlagsAround) {
                return openCellsNeighborsMiddleClick(x, y);
            }
            return new ClickResult(null, null, null, false, false, false,
                    numberOfBombs - putedFlags);
        }

        if (button == ModelButtons.LEFT_BUTTON) {
            //Если бомба при первом клике, перемещаем ее в другую клетку
            if (isInitialClick) {
                if (field[x][y].getType() == EntityType.BOMB) {
                    teleportBomb(x, y);
                }
            }
            //ЛКМ по флагу не делает ничего
            if (openedOrClosedCells[x][y].getType() == EntityType.FLAG) {
                return new ClickResult(null, null, null, false, false, false,
                        numberOfBombs - putedFlags);
            }
            //Бомба - проигрыш
            if (field[x][y].getType() == EntityType.BOMB) {
                ArrayList<Integer[]> bombsCoordinates = new ArrayList<>();
                Integer[] bomb = new Integer[2];
                bomb[0] = x;
                bomb[1] = y;
                bombsCoordinates.add(bomb);
                return new ClickResult(bombsCoordinates, null, null, false, false, false,
                        numberOfBombs - putedFlags);
            }
            //Число
            if (field[x][y].getType() == EntityType.NUMBER) {
                ArrayList<Integer[]> numberCell = new ArrayList<>();
                Integer[] coordinatesWithNumber = new Integer[3];
                coordinatesWithNumber[0] = x;
                coordinatesWithNumber[1] = y;
                coordinatesWithNumber[2] = field[x][y].getNumber();
                numberCell.add(coordinatesWithNumber);
                openedOrClosedCells[x][y].setType(EntityType.OPENED_CELL);
                return checkForWin(null, null, numberCell, false, false);
            }
            //Пустая клетка - проверяем соседей
            if (field[x][y].getType() == EntityType.OPENED_CELL) {
                return openCellsNeighborsLeftClick(x, y);
            }
        } else if (button == ModelButtons.RIGHT_BUTTON) {
            //Флаг не стоит в клетке - ставим
            if (openedOrClosedCells[x][y].getType() == EntityType.CLOSED_CELL && putedFlags < numberOfBombs) {
                openedOrClosedCells[x][y].setType(EntityType.FLAG);
                putedFlags++;
                return checkForWin(null, null, null, false, true);
            }
            //Флаг уже стоит в клетке - убираем
            if (openedOrClosedCells[x][y].getType() == EntityType.FLAG) {
                openedOrClosedCells[x][y].setType(EntityType.CLOSED_CELL);
                putedFlags--;
                return new ClickResult(null, null, null, true, false, false,
                        numberOfBombs - putedFlags);
            }
        }
        return new ClickResult(null, null, null, false, false, false,
                numberOfBombs - putedFlags);
    }

    private ClickResult checkForWin(ArrayList<Integer[]> bombCoordinates, ArrayList<Integer[]> emptyCells,
                                    ArrayList<Integer[]> numberCells, boolean removeFlag, boolean putFlag) {
        boolean isWin = true;
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; j++) {
                if (field[i][j].getType() != EntityType.BOMB && openedOrClosedCells[i][j].getType() !=
                        EntityType.OPENED_CELL) {
                    isWin = false;
                }
            }
        }
        return new ClickResult(bombCoordinates, emptyCells, numberCells, removeFlag, putFlag, isWin,
                numberOfBombs - putedFlags);
    }



    public int getNumberOfBombs() {
        return numberOfBombs - putedFlags;
    }

    public FieldEntity[][] getField() {
        return field;
    }
}
