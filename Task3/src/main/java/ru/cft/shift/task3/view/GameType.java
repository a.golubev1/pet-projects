package ru.cft.shift.task3.view;

import ru.cft.shift.task3.app.model.DifficultyType;

public enum GameType {
    NOVICE,
    MEDIUM,
    EXPERT;

    public static DifficultyType convertDifficultyFromViewToModel(GameType type) {
        switch (type) {
            case NOVICE:
                return DifficultyType.NOVICE;
            case EXPERT:
                return DifficultyType.EXPERT;
            case MEDIUM:
                return DifficultyType.MEDIUM;
            default:
                return DifficultyType.EXPERT;
        }
    }
}
