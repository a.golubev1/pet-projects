package ru.cft.shift.task3.view;

import ru.cft.shift.task3.app.model.ModelButtons;

public enum ButtonType {
    LEFT_BUTTON,
    RIGHT_BUTTON,
    MIDDLE_BUTTON;

    public static ModelButtons convertButtonFromViewToModel(ButtonType type) {
        switch (type) {
            case LEFT_BUTTON:
                return ModelButtons.LEFT_BUTTON;
            case RIGHT_BUTTON:
                return ModelButtons.RIGHT_BUTTON;
            case MIDDLE_BUTTON:
                return ModelButtons.MIDDLE_BUTTON;
            default:
                return null;
        }
    }
}
