package ru.cft.shift.task5;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class Storage {
    private final ArrayList<Resource> resourcesInStorage;
    private final int storageSize;

    private static final Logger logger = LoggerFactory.getLogger(Storage.class);

    public Storage(int storageSize) {
        resourcesInStorage = new ArrayList<>();
        this.storageSize = storageSize;
    }

    public synchronized void addResource(Resource resource, Producer producer) throws InterruptedException {
        while (resourcesInStorage.size() >= storageSize) {
            logger.info("Producer {} waits", producer.getSerialNumber());
            wait();
        }
        logger.info("Producer {} adds resource {}", producer.getSerialNumber(), resource.getId());
        resourcesInStorage.add(resource);
        notify();
    }

    public synchronized void takeResource(Consumer consumer) throws InterruptedException {
        while (resourcesInStorage.size() < 1) {
            logger.info("Consumer {} waits", consumer.getSerialNumber());
            wait();
        }
        logger.info("Consumer {} takes resource {}", consumer.getSerialNumber(), resourcesInStorage.get(0).getId());
        resourcesInStorage.remove(0);
        notify();
    }

}
