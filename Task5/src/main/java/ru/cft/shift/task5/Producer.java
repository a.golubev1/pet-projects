package ru.cft.shift.task5;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Producer implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(Producer.class);
    private static int curNumber = 0;

    private final int producerTime;
    private final Storage store;
    private final int serialNumber;

    public Producer(Storage store, int time) {
        this.serialNumber = curNumber;
        curNumber++;
        this.store = store;
        this.producerTime = time;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Resource newResource = new Resource();
            try {
                store.addResource(newResource, this);
                Thread.sleep(producerTime);
            } catch (InterruptedException e) {
                logger.error("Поток прерван", e);
                Thread.currentThread().interrupt();
            }
        }
    }

    public int getSerialNumber() {
        return serialNumber;
    }
}
