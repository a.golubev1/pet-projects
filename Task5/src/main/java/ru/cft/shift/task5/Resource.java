package ru.cft.shift.task5;

import java.util.UUID;

public class Resource {
    private UUID id = UUID.randomUUID();

    public UUID getId() {
        return id;
    }
}
