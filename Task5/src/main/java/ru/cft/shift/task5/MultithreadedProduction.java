package ru.cft.shift.task5;

import java.util.ArrayList;
import java.util.List;

public class MultithreadedProduction {
    public static void simulate(PropertyData data) {
        List<Thread> producers = new ArrayList<>();
        List<Thread> consumers = new ArrayList<>();
        Storage storage = new Storage(data.getStorageSize());
        for (int i = 0; i < data.getProducerCount(); i++) {
            Thread producerThread = new Thread(new Producer(storage, data.getProducerTime()));
            producers.add(producerThread);
        }
        for (int i = 0; i < data.getConsumerCount(); i++) {
            Thread consumerThread = new Thread(new Consumer(storage, data.getConsumerTime()));
            consumers.add(consumerThread);
        }
        producers.forEach(Thread::start);
        consumers.forEach(Thread::start);
    }
}
