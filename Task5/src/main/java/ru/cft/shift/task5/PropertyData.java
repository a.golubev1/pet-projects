package ru.cft.shift.task5;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyData {
    private final int producerCount;
    private final int consumerCount;
    private final int producerTime;
    private final int consumerTime;
    private final int storageSize;

    public PropertyData() throws IOException {
        Properties prop = new Properties();
        String propFileName = "config.properties";
        try(InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName)) {
            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }
            producerCount = Integer.parseInt(prop.getProperty("producerCount"));
            consumerCount = Integer.parseInt(prop.getProperty("consumerCount"));
            producerTime = Integer.parseInt(prop.getProperty("producerTime"));
            consumerTime = Integer.parseInt(prop.getProperty("consumerTime"));
            storageSize = Integer.parseInt(prop.getProperty("storageSize"));
        }
    }

    public int getConsumerCount() {
        return consumerCount;
    }

    public int getConsumerTime() {
        return consumerTime;
    }

    public int getProducerCount() {
        return producerCount;
    }

    public int getProducerTime() {
        return producerTime;
    }

    public int getStorageSize() {
        return storageSize;
    }
}
