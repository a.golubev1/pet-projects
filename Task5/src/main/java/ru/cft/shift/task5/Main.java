package ru.cft.shift.task5;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        try {
            MultithreadedProduction.simulate(new PropertyData());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

}
