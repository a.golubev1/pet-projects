package ru.cft.shift.task5;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Consumer implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(Storage.class);
    private static int curNumber = 0;

    private final int consumerTime;
    private final Storage storage;
    private final int serialNumber;

    public Consumer(Storage storage, int consumerTime) {
        this.serialNumber = curNumber;
        curNumber++;
        this.storage = storage;
        this.consumerTime = consumerTime;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                storage.takeResource(this);
                Thread.sleep(consumerTime);
            } catch (InterruptedException e) {
                logger.error("Поток прерван", e);
                Thread.currentThread().interrupt();
            }
        }
    }

    public int getSerialNumber() {
        return serialNumber;
    }
}
