import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.cft.shift.task2.figures.Triangle;

public class TriangleTest {

    @Test
    public void triangle_IfPassZero() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Triangle(0, 0, 1));
    }

    @Test
    public void triangle_IfPassNegative() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Triangle(-1, 2, 3));
    }

    @Test
    public void triangleSquare_IfPassNum() {
        Triangle tr = new Triangle(3, 4, 5);
        Assertions.assertEquals(6, tr.calculateSquare());
    }

    @Test
    public void trianglePerimeter_IfPassNum() {
        Triangle tr = new Triangle(3, 4, 5);
        Assertions.assertEquals(12, tr.calculatePerimeter());
    }

    @Test
    public void calculateAngle_IfPassNum() {
        Triangle tr = new Triangle(2, 3, 4);
        Assertions.assertEquals(28.95, tr.getAngles().get(0), 0.01);
        Assertions.assertEquals(46.56, tr.getAngles().get(1), 0.01);
        Assertions.assertEquals(104.47, tr.getAngles().get(2), 0.01);
    }
}
