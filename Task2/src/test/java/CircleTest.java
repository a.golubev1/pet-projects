import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.cft.shift.task2.figures.Circle;

public class CircleTest {

    @Test
    public void circle_IfPassZero() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Circle(0));
    }

    @Test
    public void circleSquare_IfPassNum() {
        Circle cir = new Circle(4);
        Assertions.assertEquals(50.26, cir.calculateSquare(), 0.01);
    }

    @Test
    public void circle_IfPassNegative() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Circle(-1));
    }

    @Test
    public void circlePerimeter_IfPassNum() {
        Circle cir = new Circle(4);
        Assertions.assertEquals(25.13, cir.calculatePerimeter(), 0.01);
    }
}
