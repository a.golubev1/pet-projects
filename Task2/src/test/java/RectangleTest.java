import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.cft.shift.task2.figures.Rectangle;

public class RectangleTest {

    @Test
    public void rectangleSquare_IfPassNum() {
        Rectangle rec = new Rectangle(4, 2);
        Assertions.assertEquals(8, rec.calculateSquare());
    }

    @Test
    public void rectanglePerimeter_IfPassNum() {
        Rectangle rec = new Rectangle(4, 2);
        Assertions.assertEquals(12, rec.calculatePerimeter());
    }

    @Test
    public void rectangle_IfPassZero() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Rectangle(0, 1));
    }

    @Test
    public void rectangle_IfPassNegative() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Rectangle(-1, 1));
    }

    @Test
    public void rectangleDiagonal_IfPassNum() {
        Rectangle rec = new Rectangle(4, 2);
        Assertions.assertEquals(4.47, rec.getDiagonal(), 0.01);
    }
}
