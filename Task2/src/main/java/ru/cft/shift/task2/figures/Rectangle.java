package ru.cft.shift.task2.figures;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintWriter;

public class Rectangle extends Figure {
    private final double length;
    private final double width;
    private final double diagonal;

    private static final Logger logger = LogManager.getLogger(Rectangle.class);

    public Rectangle(double side1, double side2) {
        logger.info("Building RECTANGLE");
        if (side1 <= 0 || side2 <= 0) {
            throw new IllegalArgumentException("Side is below or equals 0");
        }

        this.name = "Прямоугольник";
        if (side1 >= side2) {
            this.length = side1;
            this.width = side2;
        } else {
            this.length = side2;
            this.width = side1;
        }
        this.diagonal = Math.sqrt(this.length * this.length + this.width * this.width);
        logger.info("Calculating diagonal {}", this.diagonal);
    }

    @Override
    public double calculateSquare() {
        logger.info("Calculating square");
        return this.length * this.width;
    }

    @Override
    public double calculatePerimeter() {
        logger.info("Calculating perimeter");
        return 2 * (this.length + this.width);
    }

    @Override
    public void writeAboutFigureInOutput(PrintWriter writer) {
        logger.info("Writing information about figure");
        super.writeAboutFigureInOutput(writer);
        writer.println("Длина: " + this.length + measureUnits);
        writer.println("Ширина: " + this.width + measureUnits);
        writer.println("Длина диагонали: " + this.diagonal + measureUnits);
    }

    public double getDiagonal() {
        return diagonal;
    }
}
