package ru.cft.shift.task2;

import java.io.FileNotFoundException;
import java.util.NoSuchElementException;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import ru.cft.shift.task2.figures.Figure;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        InputDataFromConsole data = null;
        try {
            logger.info("Start of the programme");
            data = new InputDataFromConsole(args);
            FigureBuilder builder = new FigureBuilder();
            Figure figureFromInput = builder.buildFigure(new InputDataFromFile(data));
            figureFromInput.writeAboutFigureInOutput(data.getWriter());
            logger.info("End of the programme");
        } catch (NoSuchElementException | IllegalArgumentException | FileNotFoundException e) {
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error("Unknown error in programme: ", e);
        } finally {
            assert data != null;
            if (data.isOutputInConsole()) {
                data.getWriter().flush();
            } else {
                data.getWriter().close();
            }
        }
    }
}

