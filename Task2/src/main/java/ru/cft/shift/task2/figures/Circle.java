package ru.cft.shift.task2.figures;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintWriter;

public class Circle extends Figure {
    private final double radius;
    private final double diameter;

    private static final Logger logger = LogManager.getLogger(Circle.class);

    public Circle(double number) {
        logger.info("Building CIRCLE");
        if (number <= 0) {
            throw new IllegalArgumentException("Radius is below or equals 0");
        }
        this.name = "Круг";
        this.radius = number;
        this.diameter = 2 * number;
    }

    @Override
    public double calculateSquare() {
        logger.info("Calculating square");
        return Math.PI * this.radius * this.radius;
    }

    @Override
    public double calculatePerimeter() {
        logger.info("Calculating perimeter");
        return Math.PI * this.radius * 2;
    }

    @Override
    public void writeAboutFigureInOutput(PrintWriter writer) {
        logger.info("Writing information about figure");
        super.writeAboutFigureInOutput(writer);
        writer.println("Радиус: " + this.radius + measureUnits);
        writer.println("Диаметр: " + this.diameter + measureUnits);
    }
}
