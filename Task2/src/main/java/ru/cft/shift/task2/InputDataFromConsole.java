package ru.cft.shift.task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class InputDataFromConsole {
    private File inputFileName;
    private final PrintWriter writer;
    private boolean outputInConsole;

    private static final Logger logger = LogManager.getLogger(InputDataFromConsole.class);

    public InputDataFromConsole(String[] args) throws IOException {
        boolean repeatedArgumentForFileOrConsole = false;
        boolean flagForInput = false;
        boolean flagForOutput = false;
        String outputFileName = null;
        for (String argument : args) {
            logger.info("Reading argument {} ", argument);
            if (argument.equals("-c") && !repeatedArgumentForFileOrConsole) {
                this.outputInConsole = true;
                repeatedArgumentForFileOrConsole = true;
            } else if (argument.equals("-f") && !repeatedArgumentForFileOrConsole) {
                this.outputInConsole = false;
                repeatedArgumentForFileOrConsole = true;
            } else if (argument.equals("-i")) {
                flagForInput = true;
            } else if (flagForInput && this.inputFileName == null) {
                this.inputFileName = new File(argument);
            } else if (argument.equals("-o")) {
                flagForOutput = true;
            } else if (flagForOutput && outputFileName == null) {
                outputFileName = argument;
            }
        }

        if (!repeatedArgumentForFileOrConsole) {
            throw new IllegalArgumentException("Wrong argument for console/file output");
        }

        if (this.outputInConsole) {
            this.writer = new PrintWriter(System.out);
        } else if (outputFileName != null) {
            this.writer = new PrintWriter(outputFileName);
        } else {
            throw new IllegalArgumentException("No output file in arguments");
        }

        if (this.inputFileName == null) {
            throw new IllegalArgumentException("No input file in arguments");
        }
    }


    public PrintWriter getWriter() {
        return writer;
    }

    public boolean isOutputInConsole() {
        return outputInConsole;
    }

    public File getInputFileName() {
        return inputFileName;
    }
}
