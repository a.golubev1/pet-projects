package ru.cft.shift.task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class InputDataFromFile {
    private final String figureName;
    private final ArrayList<String> parameters;

    private static final Logger logger = LogManager.getLogger(InputDataFromFile.class);

    public InputDataFromFile(InputDataFromConsole data) throws IOException {
        if (!data.getInputFileName().exists()){
            throw new FileNotFoundException("Input file does not exist");
        }
        try (Scanner in = new Scanner(data.getInputFileName())) {
            if (!in.hasNextLine()) {
                throw new NoSuchElementException("Input file is empty");
            }
            this.figureName = in.nextLine();
            logger.info("Reading from file {} figure's name {}", data.getInputFileName(), this.figureName);
            if (!in.hasNextLine()) {
                throw new IllegalArgumentException("No parameters in input file");
            }
            this.parameters = new ArrayList<>(Arrays.asList(in.nextLine().split(" ")));
            logger.info("Reading from file {} figure's parameters {}", data.getInputFileName(), this.parameters);
        }
    }

    public ArrayList<String> getParameters() {
        return parameters;
    }

    public String getFigureName() {
        return figureName;
    }
}
