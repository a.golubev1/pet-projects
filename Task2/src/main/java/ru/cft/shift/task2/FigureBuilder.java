package ru.cft.shift.task2;

import ru.cft.shift.task2.figures.Circle;
import ru.cft.shift.task2.figures.Figure;
import ru.cft.shift.task2.figures.Rectangle;
import ru.cft.shift.task2.figures.Triangle;

import java.util.ArrayList;

public class FigureBuilder {

    private void checkParameters(ArrayList<String> numbers, int neededCountOfParameters) {
        if (numbers.size() != neededCountOfParameters) {
            throw new IllegalArgumentException("Wrong parameter's number in input file");
        }
        for (String number : numbers) {
            if (parseStringToDouble(number) < 0) {
                throw new IllegalArgumentException("Negative parameters");
            }
        }
    }

    private double parseStringToDouble(String parameterInString) {
        if (!parameterInString.matches("\\d+\\.\\d+") && !parameterInString.matches("\\d+")) {
            throw new NumberFormatException("Not a number in figure's parameters " + parameterInString);
        }
        return Double.parseDouble(parameterInString);
    }

    public Figure buildFigure(InputDataFromFile data) {
        switch (data.getFigureName()) {
            case "CIRCLE":
                checkParameters(data.getParameters(), 1);
                return new Circle(Double.parseDouble(data.getParameters().get(0)));
            case "RECTANGLE":
                checkParameters(data.getParameters(), 2);
                return new Rectangle(Double.parseDouble(data.getParameters().get(0)),
                        Double.parseDouble(data.getParameters().get(1)));
            case "TRIANGLE":
                checkParameters(data.getParameters(), 3);
                return new Triangle(Double.parseDouble(data.getParameters().get(0)),
                        Double.parseDouble(data.getParameters().get(1)),
                        Double.parseDouble(data.getParameters().get(2)));
            default:
                throw new IllegalArgumentException("No figure in input file");
        }
    }
}
