package ru.cft.shift.task2.figures;

import java.io.PrintWriter;

public abstract class Figure {
    String name;

    public static final String measureUnits = " мм";

    abstract double calculateSquare();

    abstract double calculatePerimeter();

    public void writeAboutFigureInOutput(PrintWriter writer){
        writer.println("Тип фигуры: " + this.name);
        writer.println("Площадь: " + this.calculateSquare() + measureUnits + " кв");
        writer.println("Периметр: " + this.calculatePerimeter() + measureUnits);
    }
}
