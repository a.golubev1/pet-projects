package ru.cft.shift.task2.figures;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintWriter;
import java.util.ArrayList;

public class Triangle extends Figure {
    private final ArrayList<Double> lengths;
    private final ArrayList<Double> angles;

    private static final Logger logger = LogManager.getLogger(Triangle.class);

    public Triangle(double side1, double side2, double side3) {
        logger.info("Building TRIANGLE");
        if (side1 >= side2 + side3 || side2 >= side1 + side3 || side3 >= side1 + side2) {
            throw new IllegalArgumentException("Wrong triangle's lengths");

        }

        this.name = "Треугольник";
        this.lengths = new ArrayList<>();
        this.angles = new ArrayList<>();

        this.lengths.add(side1);
        this.lengths.add(side2);
        this.lengths.add(side3);

        this.angles.add(calculateAngle(side2, side3, side1));
        this.angles.add(calculateAngle(side1, side3, side2));
        this.angles.add(180 - this.angles.get(0) - this.angles.get(1));
        logger.info("Calculating angles {}", this.angles);
    }

    private double calculateAngle(double len1, double len2, double lenOpposite) {
        return Math.toDegrees(Math.acos((len1 * len1 + len2 * len2 - lenOpposite * lenOpposite) / (2 * len1 * len2)));
    }

    @Override
    public double calculateSquare() {
        logger.info("Calculating square");
        double semiPerimeter = calculatePerimeter() / 2;
        double temp = 0;
        temp += semiPerimeter;
        for (int i = 0; i < 3; i++) {
            temp *= semiPerimeter - lengths.get(i);
        }
        return Math.sqrt(temp);
    }

    @Override
    public double calculatePerimeter() {
        logger.info("Calculating perimeter");
        double result = 0;
        for (int i = 0; i < 3; i++) {
            result += this.lengths.get(i);
        }
        return result;
    }

    @Override
    public void writeAboutFigureInOutput(PrintWriter writer) {
        logger.info("Writing information about figure");
        super.writeAboutFigureInOutput(writer);
        for (int i = 0; i < 3; i++) {
            writer.println("Длина " + (i + 1) + " стороны и противолежащий угол: " + this.lengths.get(i) + measureUnits
                    + ", " + this.angles.get(i) + " грд");
        }
    }

    public ArrayList<Double> getAngles() {
        return angles;
    }
}
