package ru.cft.shift.task4;

public class Task implements Runnable {
    private double result;
    private final int startIndex;
    private final int endIndex;

    public Task(int startIndex, int endIndex) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.result = 0;
    }

    @Override
    public void run() {
        for (int i = startIndex; i <= endIndex; i++) {
            result += 1 / (Math.pow(2, i));
        }
    }

    public double getResult() {
        return result;
    }
}
