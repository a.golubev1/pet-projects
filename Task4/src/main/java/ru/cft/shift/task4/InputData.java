package ru.cft.shift.task4;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;

public class InputData {
    private static final Logger logger = LoggerFactory.getLogger(InputData.class);

    public static int readData() {
        System.out.println("Insert number");
        Scanner input = new Scanner(System.in);
        if (!input.hasNextInt()) {
            throw new IllegalArgumentException("Not a number");
        }
        int n = input.nextInt();
        logger.info("Reading input number {}", n);
        if (n < 1) {
            throw new IllegalArgumentException("Number is below 1");
        }
        return n;
    }
}
