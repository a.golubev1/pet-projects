package ru.cft.shift.task4;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        try {
            ParallelCalculator calculator = new ParallelCalculator();
            logger.info("Answer: {}", calculator.calculate(InputData.readData()));
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
}
