package ru.cft.shift.task4;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ParallelCalculator {
    private static final int AMOUNT_OF_THREADS = 8;
    private static final int MINIMAL_INTERVAL_LENGTH = 1;
    private static final Logger logger = LoggerFactory.getLogger(ParallelCalculator.class);

    public double calculate(int n) {
        ExecutorService executor = Executors.newFixedThreadPool(AMOUNT_OF_THREADS);
        int intervalLengths = Math.max(n / AMOUNT_OF_THREADS, MINIMAL_INTERVAL_LENGTH);
        int currentIndex = 1;
        ArrayList<CompletableFuture<?>> listOfFuture = new ArrayList<>();
        ArrayList<Task> listOfTasks = new ArrayList<>();
        for (int i = 0; i < AMOUNT_OF_THREADS && currentIndex <= n; i++) {
            int end = (i != AMOUNT_OF_THREADS - 1 && currentIndex != n) ? currentIndex + intervalLengths : n;
            Task newTask = new Task(currentIndex, end);
            listOfTasks.add(newTask);
            logger.info("Running task from {} to {}", currentIndex, end);
            CompletableFuture<?> future = CompletableFuture.runAsync(newTask, executor);
            listOfFuture.add(future);
            currentIndex += intervalLengths + 1;
        }
        CompletableFuture<?> finishFuture = CompletableFuture.allOf(listOfFuture.toArray(new CompletableFuture[0]));
        finishFuture.join();
        double answer = 0;
        if (finishFuture.isDone()) {
            executor.shutdown();
            for (Task currentTask : listOfTasks) {
                answer += currentTask.getResult();
            }
            return answer;
        }
        return -1;
    }
}
